let errors = [];

function ValidationContract(){
	errors = [];
}

ValidationContract.prototype.isRequired = (value, message) => {
	if(!value || value.length <= 0)
		errors.push({ message:message});
}

ValidationContract.prototype.hasMinLen = (value, min, message) => {
	if(!value || value.length < min)
		errors.push({ message: message});

}

ValidationContract.prototype.hasMaxLen = (value, max, message) => {
        if(!value || value.length > max)
                errors.push({ message: message});

}

ValidationContract.prototype.hasFixedLen = (value, len, message) => {
        if(value.length  !== len)
                errors.push({ message: message});

}

ValidationContract.prototype.isEmail = (value,  message) => {
        const reg = new RegExp(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/);
	if(!reg.test(value))
                errors.push({ message: message});

}


ValidationContract.prototype.isUrl = (url,message) => {
	const reg = new RegExp(/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!]))?/)

	if(!reg.test(url))
		errors.push({ message: message});
}

ValidationContract.prototype.errors = () => {
	return errors;
}

ValidationContract.prototype.clear = () => {
        errors = [];
}

ValidationContract.prototype.isValid = () => {
	return errors.length === 0;
}

module.exports = ValidationContract;
