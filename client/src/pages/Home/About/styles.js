import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  root: {
    flexGrow: 1,
    height:'70vh',
    top:0,
    margin:0,
    padding:0,
    width:'100%',
    display:'flex',
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'space-around',
    backgroundSize:'cover',
    backgroundImage:'url("https://cdn.pixabay.com/photo/2015/12/01/15/43/black-1072366__340.jpg")',
  },
  title:{
    flexGrow: 1,
    marginTop:'20px',
    textAlign:'center',
    color:'white',
  },

}));