import React, { Component } from 'react';
import Header from '../../components/Header';
import About from './About';
import Link from './Link';
import WhyUs from './WhyUs';
import Footer from '../../components/Footer';

class Home extends Component {
	render(){
		return(
				<div style={{display:'flex',flexDirection:'column',alignItems:'center'}} >
					<Header/>
					<About/>
					<Link/>
					<WhyUs/>
					<Footer/>
				</div>
			)
	}
}

export default Home;
