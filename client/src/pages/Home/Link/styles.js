import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
 paper:{
   width:'80%',
   height:'100px',
   zIndex:2,
   marginTop:'-50px',
   display:'flex',
   flexDirection:'row',
   alignItems:'center',
   justifyContent:'space-around',
 },
 textField:{
   width:'60%'
 },
 btn:{
   width:'20%',
   fontSize:'25px'
 }
}));