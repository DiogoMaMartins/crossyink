import React, { useState } from 'react';
import useStyles from './styles';
import { Paper,TextField,Button,IconButton } from '@material-ui/core';
import Copy from '@material-ui/icons/FileCopy';
import Replay from '@material-ui/icons/Replay';
import Swal from 'sweetalert2'

// import validator
import ValidationContract from '../../../validators';


//Redux

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as linkActions from '../../../redux/actions/link';


function Link({link,generateShortUrl,restartShortUrl,errorUrl}){
	const classes = useStyles();
	const [ originalLink,setoriginalLink ] = useState(link.originalUrl);
	const [ showSortedLink,setshowSortedLink ] = useState(false);
	

	const generate = async ()  => {

		//to do verify if is a valable url

		let contract = await new ValidationContract();

			await contract.isUrl(originalLink," Provide a valide url!")

			 if  (await !contract.isValid()){
					//errorUrl()

				Swal.fire({
				  type: 'error',
				  title: JSON.stringify(contract.errors()[0].message, null, 4).replace(/['"]+/g, ''),
				  html:
					"Don't forget to provide <b>Http or Https</b>, ",
				  animation: false,
				  customClass: {
				    popup: 'animated tada'
				  }
				})


			
				
			}

			await generateShortUrl(originalLink)

				 if ( await contract.isValid()){
					setshowSortedLink(true)
				}
	}

	const restart = () => {
		restartShortUrl()
		setshowSortedLink(false)
	}

	return(
		<>
		{
			showSortedLink  ?
			<Paper className={classes.paper} >

			 <TextField
		        id="outlined-shortedLink-input"
		        label="Shorted Link"
		       
		        value={link.shortUrl}
		        className={classes.textField}
		        type="shortedLink"
		        name="shortedLink"
		        autoComplete="shortedLink"
		        margin="normal"
		        variant="outlined"
		      />

		      <IconButton onClick={() => {navigator.clipboard.writeText(link.shortUrl)}} >
		      	<Copy style={{color:'#007CFE'}} />
		      </IconButton>

		      <IconButton onClick={() => restart()}  >
		      	<Replay style={{color:'#007CFE'}} />
		      </IconButton>

		       </Paper>

			:

			<Paper className={classes.paper} >

			 <TextField
		        id="outlined-link-input"
		        label="Link"
		        className={classes.textField}
		        type="link"
		        name="link"
		        value={originalLink}
		        onChange={e => setoriginalLink(e.target.value)}
		        autoComplete="link"
		        margin="normal"
		        variant="outlined"
		      />

		       <Button variant="contained" style={{backgroundColor:'#007CFE',color:'white'}} className={classes.btn} onClick={() => generate()}>Shorten</Button>
			</Paper>
		}
		</>
		)
}


const mapStateToProps = state => ({
	link: state.link,
})

const mapDispatchToProps = dispatch => bindActionCreators(linkActions,dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(Link);

