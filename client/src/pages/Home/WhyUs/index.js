import React from 'react';
import useStyles from './styles';

import { Card, CardActionArea, CardContent,Typography,CardMedia,Grid} from '@material-ui/core';

export default function WhyUs(){
	const classes = useStyles();
	return(
		<div className={classes.root}>


			 <Typography gutterBottom variant="h2" component="h2" className={classes.title} >
				            Why us ?
			</Typography>

			 <Grid container spacing={2} align="center" className={classes.containerGrid} >
			 	<Grid item xs={12} md={4} lg={4}>
					<Card className={classes.card} >
				      <CardActionArea>
				        <CardMedia
				        className={classes.media}
				         
				          image="https://www.sysaid.com/wp-content/uploads/features/analytics/dashboard.jpg"
				          title="Contemplative Reptile"
				        />
				        <CardContent>
				          <Typography gutterBottom variant="h5" component="h2">
				            Lizard
				          </Typography>
				           <Typography variant="body2" color="textSecondary" component="p">
				            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
				            across all continents except Antarctica
				          </Typography>
				        </CardContent>
				     </CardActionArea>
				    </Card>
			    </Grid>

			    <Grid item xs={12} md={4} lg={4}>
					<Card className={classes.card} >
				      <CardActionArea>
				        <CardMedia
				        className={classes.media}
				         
				          image="https://www.sysaid.com/wp-content/uploads/features/analytics/dashboard.jpg"
				          title="Contemplative Reptile"
				        />
				        <CardContent>
				          <Typography gutterBottom variant="h5" component="h2">
				            Lizard
				          </Typography>
				           <Typography variant="body2" color="textSecondary" component="p">
				            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
				            across all continents except Antarctica
				          </Typography>
				        </CardContent>
				     </CardActionArea>
				    </Card>
			    </Grid>

			    <Grid item xs={12} md={4} lg={4}>
					<Card className={classes.card} >
				      <CardActionArea>
				        <CardMedia
				        className={classes.media}
				         
				          image="https://www.sysaid.com/wp-content/uploads/features/analytics/dashboard.jpg"
				          title="Contemplative Reptile"
				        />
				        <CardContent>
				          <Typography gutterBottom variant="h5" component="h2">
				            Lizard
				          </Typography>
				           <Typography variant="body2" color="textSecondary" component="p">
				            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
				            across all continents except Antarctica
				          </Typography>
				        </CardContent>
				     </CardActionArea>
				    </Card>
			    </Grid>
		    </Grid>
		</div>
		)
}