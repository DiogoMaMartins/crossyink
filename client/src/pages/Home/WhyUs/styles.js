import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
 root: {
    flexGrow: 1,
  },
  containerGrid:{

  },
  title:{
  	textAlign:'center',
  	marginTop:10
  },
  card: {
    width: 345,
    height:400,
    margin:20
  },
  media: {
    height: 140,
  },
});