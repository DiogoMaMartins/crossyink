import React, { Component } from 'react';
import Header from '../../components/Header';
import SignUpForm from './SignUpForm';

import Footer from '../../components/Footer';

class SignUp extends Component {
	render(){
		return(
				<div style={{display:'flex',flexDirection:'column',alignItems:'center'}} >
					<Header/>
						<SignUpForm/>
					<Footer/>
				</div>
			)
	}
}

export default SignUp;
