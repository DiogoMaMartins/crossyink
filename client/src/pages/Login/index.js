import React, { Component } from 'react';
import Header from '../../components/Header';
import LoginForm from './LoginForm';

import Footer from '../../components/Footer';

class Login extends Component {
	render(){
		return(
				<div style={{display:'flex',flexDirection:'column',alignItems:'center'}} >
					<Header/>
						<LoginForm/>
					<Footer/>
				</div>
			)
	}
}

export default Login;
