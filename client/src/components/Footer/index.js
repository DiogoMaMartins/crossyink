import React from 'react';
import useStyles from './styles';
import Message from '@material-ui/icons/Message';

export default function Footer(){
	const classes = useStyles();

	return(
		<footer className={classes.footer} >
			<div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-around'}} >
				<Message style={{color:'white'}}/>
				<h1>contact@crossyink.tk</h1>
			</div>
			

			<span>Copyright © {new Date().getFullYear()} Crossyink</span>
		</footer>
		)
}