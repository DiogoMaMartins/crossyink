import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
 footer:{
   flexGrow: 1,
   width:'100%',
   height:'200px',
   backgroundColor:'#1A1D2D',
   color:'white',
   display:'flex',
   flexDirection:'column',
   alignItems:'center',
   justifyContent:'center'
 }
}));