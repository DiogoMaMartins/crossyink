import React from 'react';
import useStyles from './styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Sync from '@material-ui/icons/Sync';


import {Link} from 'react-router-dom';

export default function Header(){
	const classes = useStyles();


	return(
		<div className={classes.root}>
			<AppBar position="static"  style={{ background: '#fff',color:'#000' }} >
				<Toolbar>
					<Sync style={{color:'#7012AB',fontSize:45}} />
		          <Typography variant="h4" className={classes.title}>
		          <Link  style={{color:"#7012AB"}} to="/">Crossyink</Link>
		            
		          </Typography>
		          	<section style={{display:'flex',minWidth:'20%',flexDirection:'row',alignItems:'center',justifyContent:'space-around'}} >
			          <Button variant="outlined" color="inherit" ><Link  style={{color:"black"}} to="/login">Login</Link></Button>
			          <Button variant="contained" style={{backgroundColor:'#60C260',color:'white',marginLeft:'10px'}} className={classes.signup} ><Link  style={{color:"white"}} to="/signUp">SignUp</Link></Button>
        			</section>
        		</Toolbar>
			</AppBar>
			
		</div>
		)
}