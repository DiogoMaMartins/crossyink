import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  root: {
    flexGrow: 1,
    margin:0,
    padding:0,
    width:'100%'
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  signup:{
  	backgroundColor:'#60C260',
  },
  title: {
    flexGrow: 1,
    color:'#7012AB'
  },
}));