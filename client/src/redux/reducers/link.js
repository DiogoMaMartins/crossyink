const INITIAL_STATE = {
	shortUrl:'',
	originalUrl:'',
	loading:false,
	error:false,
}



export default function link(state = INITIAL_STATE, action) {
	switch (action.type){
		case 'GENERATE_SHORT_URL':
			return { ...state, originalUrl:action.payload.originalUrl, loading:true };
		case 'SUCCESS_SHORT_URL':
			return { ...state,shortUrl:action.payload.shortUrl, loading:false, error:false };
		case 'ERROR_SHORT_URL':
		
			 let newState = { ...state, loading:false,error:true };
			 return newState;

		case 'RESTART_SHORT_URL':
			return { originalUrl:'',shortUrl:'',loading:false,error:false };

		default:
			return state;
	}
}