export function generateShortUrl(originalUrl){
	return{
		type: 'GENERATE_SHORT_URL',
		payload:{
			originalUrl
		}
	}
}

export function errorUrl(){
	return {
		type:'ERROR_SHORT_URL'
	}
}

export function restartShortUrl(){
	return {
		type:'RESTART_SHORT_URL',
	}
}