import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import link from '../reducers/link';
import rootSaga from '../sagas';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
		combineReducers({
			link,
		}),
		applyMiddleware(sagaMiddleware)
	);


sagaMiddleware.run(rootSaga);
export default store;