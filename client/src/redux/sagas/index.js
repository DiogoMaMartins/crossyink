import { takeLatest, put, call,all } from 'redux-saga/effects';
import axios from 'axios';


function apiGet(originalUrl) {
  return axios.post('http://localhost:3001/generateLink',{
    originalUrl
  }).then(response => {
    return response.data.shortUrl
  }).catch(err => {return err})

  /*axios.post('http://localhost:3001./',{
    originalUrl
  }).then(response => {
    console.log("response",response.data)
  })
  .catch(err => {
    return err
  })*/
} 

/*function apiCreateUrl() {
  let responseData;
  axios.post('https://localhost:3001/',{
    originalUrl
  }).then(response => {
    responseData = response.data;
  })
  .catch(err => {
    return err
  })


  return new Promise((resolve, reject) => {
     resolve(
        responseData.shortUrl,
      );
  });
}*/
 function* generateUrl(action) {
  try{
    const response = yield call(apiGet,action.payload.originalUrl);

    if(response.err === "Err"){
     return false
    }else{
       yield put({ type: 'SUCCESS_SHORT_URL', payload: { shortUrl: response } });
    }
   
  
  }catch(err){
    yield put({ type: 'ERROR_SHORT_URL'});
  }
}


function* actionWatcher(){
  yield takeLatest('GENERATE_SHORT_URL', generateUrl)
}

export default function* root(){
  yield all([
      actionWatcher(),
    ])
}