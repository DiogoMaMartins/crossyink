import React, { Component } from 'react';
import { Route,Switch,Redirect,BrowserRouter as Router } from 'react-router-dom';
import {Animated} from "react-animated-css";
import { isAuthenticated } from './auth';


//import pages

import Home from '../pages/Home';
import Login from '../pages/Login';
import SignUp from '../pages/SignUp';

import Dashboard from '../pages/connected/Dashboard';



const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
  {...rest}
    render={props =>
    isAuthenticated() ? (
      <Component {...props}/>
      ): (
      <Redirect to={{ pathname: '/', state: { from: props.location }}}/>
    )
    }
    />
  );



class RedirectUrl extends Component {

  async componentWillMount (){
      
  }

  render(){
    const code =  this.props.match.params.code
    window.location.href = `http://localhost:3001/${code}`;
    return(
      <div>
      <Animated animationIn="bounceInLeft" animationOut="fadeOut" isVisible={true}>
        <div>
            Redirect ;)
        </div>
    </Animated>
    </div>
      )
  }
}
class Routes extends Component{
  render(){
    return(
      <Router>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/signUp" component={SignUp}/>
          <Route exact path="/:code" component={RedirectUrl}/> 
          
          <PrivateRoute exact path="/dashboard" component={Dashboard}/>
        </Switch>
      </Router>
    )
  }
}

export default Routes;