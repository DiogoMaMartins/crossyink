const mongoose = require('mongoose');
const Customers = mongoose.model('Customers');


exports.register = async(data) => {
	let login = await new Customers(data);
	await login.save();
}


exports.login = async(data) => {
	const response = await Customers.findOne({
		email:data.email,
		password:data.password
	});
	return response;
}