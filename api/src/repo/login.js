const mongoose = require('mongoose');
const Login = mongoose.model('Login');


exports.register = async(data) => {
	let login = await new Login(data);
	await login.save();
}


exports.login = async(data) => {
	const response = await Login.findOne({
		loginName:data.loginName,
		password:data.password
	});
	return response
}