const mongoose = require('mongoose');
const Link = mongoose.model('Link');

exports.save = async(data) => {
	let link = await new Link(data);
	await link.save();

	return link
}

exports.findUrl = async(code) => {
	let link = await Link.findOne({
		urlCode:code
	})

	return link
}
exports.find = async(originalUrl) => {
	let link = await Link.findOne({
		originalUrl:originalUrl
	})

	return link;
}