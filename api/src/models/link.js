const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
	originalUrl: String,
	urlCode: String,
	shortUrl: String,
	createdAt: { 
		type: Date, 
		default: Date.now
		 },
	updatedAt: {
	 type: Date,
	  default: Date.now
	   }
});

module.exports = mongoose.model('Link',schema);