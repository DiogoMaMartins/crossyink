'use strict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/link');

router.get('/:code',controller.redirect);

router.post('/generateLink',controller.generate);
module.exports = router;
