'use strict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/login');
const authservice = require('../middleware/auth');

//router.post('/signup',controller.register);
router.post('/signin',controller.login);

module.exports = router;