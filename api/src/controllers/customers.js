'use strict'

const repository = require('../repo/customers.js');
const md5 = require('md5');
const authService = require('../middleware/auth');
const ValidationContract = require('../validators/validators');


exports.register = async(req,res,next) => {
	try{

		let { firstname,lastname,email,password } = req.body;

		await repository.register({
			firstname,
			lastname,
			email,
			password			
		})

		res.status(201).send({
			message:"Welcome!"
		})

	}catch(error){
		res.status(500).send({
			message:'Error',
			error
		})
	}
}


exports.login = async(req,res,next) => {
	try{
		let { email,password } = req.body;
		
		const login = await repository.login({
			email,
			password:md5(password + global.SALT_KEY)
		})

		if(!login){
			res.status(404).send({
				message:"Email or password invalid!"
			})
			return;
		}else{
			const token = await authService.generateToken({
				id:login._id,
				email:login.email
			})

			res.status(200).send({
				token,
				data:{
					email:login.email
				}
			});
		}


	}catch(error){
		res.status(500).send({
			message:'Error',
			error
		})
	}
}