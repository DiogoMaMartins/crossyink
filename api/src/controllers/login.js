'use strict'

const repository = require('../repo/login.js');
const md5 = require('md5');
const authService = require('../middleware/auth');
const ValidationContract = require('../validators/validators');

exports.login = async(req,res,next) => {
	try{
		const login = await repository.login({
			loginName:req.body.loginName,
			password:md5(req.body.password + global.SALT_KEY)
		});

		if(!login){
			res.status(404).send({
				message:"Email or password invalid"
			})
			return;
		}

		const token = await authService.generateToken({
			id:login._id,
			loginName:login.loginName,
		})

		res.status(201).send({
			token:token,
			data:{
				loginName:login.loginName,
			}
		});
	}catch(e){
		res.status(500).send({
			message:'error',
			data:e
		})
	}
}


/*exports.register = async(req,res,next) => {
	try{

		let contract = new ValidationContract();
		  contract.hasMinLen(req.body.loginName,3,"Please your loginName need more then 3 caracters");
		  contract.hasMinLen(req.body.password,3,"Please your firstName need more then 3 caracters");

	if (!contract.isValid()){
		res.status(400).send(contract.errors()).end();
		return;
	}

		await repository.register({
			loginName:req.body.loginName,
			password:md5(req.body.password + global.SALT_KEY),
		})

		res.status(500).send({
			message:"User registered "
		})

	}catch(e){
		res.status(500).send({
			message:'Error',
			data:e
		})
	}
}*/