'use strict'

const repository = require('../repo/link.js');
const validUrl = require("valid-url");
const shortId = require("shortid");


exports.redirect = async(req,res,next) => {
	try{

		const code = req.params.code;
		const link = await repository.findUrl(code);

		if(link) {
			return res.redirect(link.originalUrl);
		}else {
			return res.send(
				"Sorry but i don't find this url"
			)
		}

	}catch(err){
		res.status(401).send({
			message:'Error',
			error:err
		})
	}
}

exports.generate = async(req,res,next) => {

	const { originalUrl } = req.body;


	try{

		// validation of url
		if( validUrl.isUri(originalUrl)){
	
			
		// generate the code to replace in url
		const code = shortId.generate();
		//watch if we have already in database

		const link = await repository.find(originalUrl)


		if(link){

			res.send(link)
			//res.status(200).json(link);
		}else{
			const  shortUrl = `http://localhost:3000/${code}`;
			
			//save in database 

			let newLink = await repository.save({
				originalUrl,
				urlCode:code,
				shortUrl
			})

			await console.log("new link",newLink)

			res.status(201).json(newLink)
		}

		}else{
			res.send({
				message:"Error Invalide Url",
				err:"Err"
			})
		}



	}catch(err){
		res.status(500).send({
				message:"Error Invalide Url",
				err:"Err"
			})
	}
}