'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const router = express.Router();
const mongoose = require('mongoose');
const config = require('../bin/config');
const cors = require('cors');

mongoose.connect(config.database,{
	keepAlive: true, 
	useNewUrlParser:true,
	useUnifiedTopology: true
})
.then(() => console.log('MongoDB Connected'))
.catch(err => console.log("err",err))

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
app.set('superNode-auth', config.configName);

app.use(cors())

app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,x-access-token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});


app.use(bodyParser.urlencoded({ extended:false}));


const Login = require('./models/login');

const logins = require('./routes/login');

const Link = require('./models/link');

const links = require('./routes/link');

app.use('/auth',logins);
app.use('/',links)



module.exports = app;
